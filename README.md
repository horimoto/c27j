# C27J

OpenMediaVaultを使用してNAS(ファイルサーバ)を作るときにRAIDボードにRocketRAID 2710を使うときの方法について記録する。

だいたい、RAID10で構成する。したがって、OS用HDD 1個、DATA用HDD 4個で構成する。

RocketRAID 2710のF/Wのバージョンを確認する。だいたい、1.5などと古いバージョンになっている。古いバージョンだと、大きなHDDが認識されなかったりするので、最新のものにupdateする。

[RocketRAID 272x_1xのFirmware(BIOS)](http://www.highpoint-tech.com/USA_new/series_rr272x_configuration.htm)

上のリンク先でRAID BIOSを選択する。ZIPアーカイブされたファイルの中にRR2710.xxx (xxxはバージョン番号)のファイルがある。これをLOAD.EXEというDOSのプログラムでアップデートさせる。

そのためにUSBメモリなどにFREEDOSをインストールしておく。このUSBメモリで起動してアップデートする。



```
#!shell

sudo apt-get install build-essential checkinstall
```

これで、OSのファームウェアをアップデートするのに必要なコンパイル材料が揃う。

RocketRAIDのサイトからFirmwareのソースコードを入手する。

[この画面のLinux Software PackageからLinux Open Source Driverを選択する](http://www.highpoint-tech.com/USA_new/series_rr272x_configuration.htm)

[RR272x_1x_Linux_Src_v1.5.18_15_04_21.tar.gz](http://www.highpoint-tech.com/BIOS_Driver/rr272x_1x/Linux/RR272x_1x_Linux_Src_v1.5.18_15_04_21.tar.gz)

```
#!shell

tar xvzf RR272x_1x_Linux_Src_v1.5.18_15_04_21.tar.gz
sudo ./RR272x_1x_Linux_Src_v1.5.18_15_04_21.bin
```

mvsasドライバの起動を抑止するためにblacklistに登録する。

```
#/etc/modprobe.d/mvsas-blacklist.conf 
blacklist mvsas
blacklist scsi_transport_sas
blacklist libsas
```

/etc/hptcfgにドライバの種類を設定する。

hptraidconfコマンドをつかってワーニングメールの送り先を設定する。

```
HPT CLI>mail server set jcloud.holly-linux.com 587 0 e c27j03raid@holly-linux.com c27j03@holly-linux.com hollyc27j03
HPT CLI>mail server

ServerAddress       Port   ssl  Status   Mail From           User Name
------------------------------------------------------------------------------
jcloud.holly-linux.com587    0    Enabled  c27j03raid@holly-linux.comc27j03@holly-linux.com

HPT CLI>mail recipient add RAID horimoto@holly-linux.com Inf War Err
HPT CLI>mail recipient

ID   Name      Mail  Address                           Notify Types
------------------------------------------------------------------------------
1    RAID      horimoto@holly-linux.com                Information Warning Error

```

